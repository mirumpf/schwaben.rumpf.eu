---
title: "Status"
permalink: /status/
layout: single
author_profile: true
comments: false
---
Mit dem Ziel unseren Kindern eine lebenswerte Umwelt zu hinterlassen, haben wir unsere Lebensweise angepasst. Das Ziel ist die Vermeidung von Müll, insbesondere Plastikmüll, den Ressourcenverbauch drastisch zu minimieren und insgesamt nachhaltiger zu Leben.
Die folgende Aufstellung zeigt den aktuellen Stand dieser Transformation, insbesondere was wir schon erreicht haben.

Viele Tips & Tricks gibt es auf folgenden Seiten:
* [Smarticular](https://www.smarticular.net/)
* [Utopia](https://utopia.de/)

## Strom

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Hausstrom | &#128994; seit März 2019 | [EWS Schönau](https://www.ews-schoenau.de/) + 2 [Sonnencent](https://www.ews-schoenau.de/unser-foerderprogramm/)/kWh | Wechsel von EnBW zur EWS Schönau (inkl. 2 Sonnencent zur Förderung von Energieeffizienz- und Bürgerenergieprojekten) |
| S0 Stromzähler | &#128994; seit 2019 | 2x [DRT428DCV2](https://bg-etech.de/download/manual/DRT428DCV2.pdf)| Aufzeichnung der Verbrauchswerte zur Erkennung von Ineffizienzen |
| Hausstrom (Eltern) | &#128994; ab Januar 2021 | [EWS Schönau](https://www.ews-schoenau.de/) | Wechsel von Innogy zur EWS Schönau |
| Wärmestrom | &#128994; seit Oktober 2019 | [EWS Schönau](https://www.ews-schoenau.de/) + 2 [Sonnencent](https://www.ews-schoenau.de/unser-foerderprogramm/) | Wechsel von EnBW zur EWS Schönau (inkl. 2 Sonnencent zur Förderung von Energieeffizienz- und Bürgerenergieprojekten) |
| Photovoltaik (Eltern) | &#128994; | Seit 2011 | 9,1 kWp |
| Photovoltaik (Schwiegereltern) | &#128994; | Seit 2017 | xx kWh |
| Photovoltaik | &#128308; | In Planung | [Beratungsgespräch](https://www.verbraucherzentrale-bawue.de/energieberatung-bw) mit der Verbraucherzentrale hat stattgefunden, nächster Schritt: Angebote einholen |
| Batteriespeicher | &#128308; in Planung |  | [Beratungsgespräch](https://www.verbraucherzentrale-bawue.de/energieberatung-bw) mit der Verbraucherzentrale hat stattgefunden, nächster Schritt: Angebote einholen |
| Wallbox | &#128308; in Planung |  | V2H wäre interessant, leider können das aktuell nur wenig E-Autos |


## Heizung

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Erdwärmepumpe | &#128994; seit März 2009 | [Vitocal 222-G](https://www.viessmann.de/de/wohngebaeude/waermepumpe/sole-wasser-waermepumpen/vitocal-222-g.html) | Es handelt sich um eine erste Version (2008) der Sole-/Wasser-Wärmepumpen von Viessmann. Mit 4300kWh Stromverbrauch für die Wärmepumpe im Jahr liegen wir mit 200m² Wohn- & Nutzfläche bei 21kWh/(m²·a) (Kilowattstunden pro Quadratmeter und Jahr). |
| Zirkulationspumpe | &#128308; |  | Was bringt eine Pumpe wie [Zirkomat](http://www.zirkomat.de/)? |


## Wasser

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| S0 Wasserzähler | &#128994; | [Kaltwasserzähler](https://www.stark-elektronik.de/epages/63653058.sf/de_DE/?ObjectPath=/Shops/63653058/Products/ETKEAX-R12G34-80-S0) | Aufzeichnung der Verbrauchswerte zur Erkennung von Ineffizienzen |
| Duschkopf | &#128994; seit August 2020 | [Bubble Rain](https://www.wolf-aqua-manufaktur.de/html/bubblerain.html) | Reduktion der Durchflußmenge auf 6L/min. |


## Telefon

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Mobilfunk | &#128308; in 2020 | [Wetell](https://wetell-change.de/) | Wechsel von der Telekom zu Wetell steht an. Wir haben 2 Gutscheine während der [Startnext Crowd-Funding](https://www.startnext.com/wetell) Kampagne erworben und warten darauf, dass der offizielle Betrieb losgeht, um die Telekomverträge zu kündigen. |
| Prepaid | &#128308; | [discoTEL](https://www.discotel.de/) | Aktuell bietet Wetell keinen Prepaid Tarif an. |
| Festnetz & Internet | &#128308; | Telekom | Gibt es eine nachhaltige Alternative? |
| Mobiltelefon | &#128992; | [Samsung S8+](https://www.samsung.com/de/smartphones/galaxy-s8/) (mein aktuelles Mobiltelefon) / [Fairphone 3](https://www.fairphone.com/en/) (das aktuelle Mobiltelefon meiner Frau) | [Shiftphone](https://www.shiftphones.com/) (mein nächstes Mobiltelefon) |


## Internet

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Website rumpf.eu | &#128994; seit Februar 2019 bis Februar 2020| [BIOHOST](https://www.biohost.de/) | Wechsel zu BIOHOST nachdem ich auf der Wetell Seite auf diesen Hoster aufmerksam geworden bin. Wechsel zu [Hostsharing eG](https://www.hostsharing.net/) ab Februar 2020, da mir das Wirtschaften noch nachhaltiger zu sein scheint, indem nicht nur der Strom, sondern auch die Hardware in die Nachhaltigkeitsbetrachtung mit einbezogen wird. |
| Website rumpfonline.de | &#128308; | 1&1 | Umzug zu [Hostsharing eG](https://www.hostsharing.net/) ausstehend. |
| Email | &#128994; seit 2016 | [Posteo](https://posteo.de) | Grün, sicher, werbefei! |
| eBooks | &#128994; | [eBook.de](https://ebook.de), [Manning](https://livebook.manning.com/#!//library), [Leanpub](https://leanpub.com/user_dashboard/library), (Vermeidung von [Amazon](https://lesen.amazon.de/)) | Ich gehe davon aus, dass alle Bücher digital erstellt werden und ein eBook deutlich weniger Ressourcen benötigt, als ein gedrucktes Buch. |
| TV | &#128994; | [waipu.tv](https://www.waipu.tv/) | Siehe [Streamen doch nicht so klimaschädlich wie angenommen](https://www.spiegel.de/wissenschaft/technik/streaming-ist-doch-nicht-so-klimaschaedlich-wie-angenommen-a-71796dd2-ec7b-446a-9f6c-f94cd5cd0712) |
| Filme & Serien | &#128994; | [Amazon Prime Video](https://www.amazon.de/Prime-Video/b?node=3279204031), [Netflix](https://www.netflix.com/de/) | Siehe [Streamen doch nicht so klimaschädlich wie angenommen](https://www.spiegel.de/wissenschaft/technik/streaming-ist-doch-nicht-so-klimaschaedlich-wie-angenommen-a-71796dd2-ec7b-446a-9f6c-f94cd5cd0712) |
| Musik | &#128994; seit Juli 2019 | [Spotify](https://www.spotify.com/de/) | Siehe [Streamen doch nicht so klimaschädlich wie angenommen](https://www.spiegel.de/wissenschaft/technik/streaming-ist-doch-nicht-so-klimaschaedlich-wie-angenommen-a-71796dd2-ec7b-446a-9f6c-f94cd5cd0712) |
| Search | &#128994; seit Juli 2019 | [Ecosia](https://www.ecosia.org/) | [How Ecosia works](https://info.ecosia.org/what) |


## Finanzen

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Girokonto | &#128994; seit Juli 2019| [Ethikbank](https://www.ethikbank.de/) | Die EthikBank ist eine ethisch-ökologische Direktbank. |
| Sparkonto | &#128308; seit 2000 | [Postbank](https://www.postbank.de/) | Gibt es Alternativen? |
| Versicherung | &#128308; | [ver.de](https://www.ver.de/) | Leider ist ver.de noch keine Versicherung, weil die BaFin Zulassung fehlt. |
| Investment | &#128308; |  | [Divestment](https://gofossilfree.org/de/divestment/)? Bisher noch keine Übersicht zu nachhaltigen Finanzprodukten gefunden... |
| Altersvorsorge | &#128308; |  | Wie baut man sowas um, ohne massive Verluste zu machen? |


## Urlaub

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Flug | &#128994; seit September 2013 | / | Verzicht auf private Flugreisen seit 2013 |
| Auto | &#128308; seit 2014 | [Climatefair](https://climatefair.de) | Urlaub mit dem Auto an der Nordsee, in Italien oder in Österreich. Kompensation via [Climatefair](https://climatefair.de) seit 2019. Anfrage bzgl. Kompensationsmöglichkeiten in Böblingen hat leider keine lokalen Alternativen ergeben. |


## Arbeit

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Fahrtweg | &#128994; seit Juni 2019 |  | Überwiegend mit dem Fahrrad (Einfache Strecke 7,8km/2,0km) |
| Dienstreise Flugzeug | &#128994; seit März 2019 | [Atmosfair](https://www.atmosfair.de) | Vermeidung von Flugreisen. Private Kompensation via Atmosfair. |
| Kantine | &#128994; seit Juni 2019 |  | Überwiegend vegetarisches Menü. |


## Mobilität

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Toyota Avensis Bj. 2008 (privat) | &#128994; seit April 2019 |  | Abgeschafft! |
| VW Bus T5 Bj. 2003 (privat) | &#128994; seit Oktober 2019 |  | Abgeschafft! |
| Mercedes V-Klasse (Firmenwagen) | &#128308; ab Oktober 2019 |  | T5 VW Bus wurde durch eine Mercedes V-Klasse mit kleinster Motorisierung ersetzt. Ziel [EQV](https://www.mercedes-benz.com/de/fahrzeuge/personenwagen/v-klasse/concept-eqv/)! Fahrten jedoch nur noch wenn unbedingt nötig. |
| Bus & S-Bahn | &#128994; | [VVS](https://www.vvs.de/) | Bei Fahrten nach Stuttgart |
| Fahrrad | &#128994; |  | Bei Fahrten in Böblingen |
| Lastenanhänger | &#128308; | [Roland Carrie M.e](https://roland-werk.de/fahrradanhaenger/carrie-m-e/) | Angedacht. Als Kompromiss wird aktuell ein Croozer Kinderanhänger "verwendet", bis die Kinder auch weitere Strecken alleine fahren können. |



## Lebensmittel

Lebensmittel bevorzugt im Glas, statt in Dosen oder Plastikbeuteln.

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Fleisch & Wurst | &#128994; deutlich reduzierter Konsum seit April 2019 | [Metzgerei Herrmann](http://www.metzgerei-hermann.de/) | Ohne Plastikverpackung |
| Gemüse und Obst | &#128994; seit April 2019 | [Wochenmarkt](https://www.lrabb.de/,Lde/start/Service+_+Verwaltung/Wochenmaerkte.html) | Ohne Plastikverpackung |
| Käse | &#128994; seit April 2019 | [Wochenmarkt](https://www.lrabb.de/,Lde/start/Service+_+Verwaltung/Wochenmaerkte.html) | Ohne Plastikverpackung |
| Butter | &#128994; seit April 2019 | [Wochenmarkt](https://www.lrabb.de/,Lde/start/Service+_+Verwaltung/Wochenmaerkte.html) | Ohne Plastikverpackung |
| Milch | &#128994; seit April 2019 | [Schönbuch Milch](https://www.schoenbuchmilch.de/) | Ohne Plastikverpackung zum selbst zapfen am Automaten |
| Kaffee | &#128994; seit April 2019 | [Fairtrade Kaffee](https://www.frech-bb.de/122/kaffee-roesterei/kaffee-sorten) | Ohne Plastikverpackung: Bohnen, Mühle und Zubereitung mit French Press |
| Müsli, Nüsse, Nudeln | &#128994; seit April 2019 | [Mein Müsli Laden](https://mein-muesli-laden.de/) | In Papiertüten verpackt. |
| Tee | &#128994; seit April 2019 | [Tee Gschwender](https://www.teegschwendner.de/) | Loser Tee in Dose |
| Tiefkühlgemüse | &#128994; seit April 2019 |  | Nur in Papierverpackung |
| Bäcker | &#128994; seit April 2019 | | Einkauf mit Stoffbeutel statt Papiertüten |
| Eier | &#128994; seit April 2019 | | Bio-Eier, Wiederverwendung der Eierkartons |
| Nuss-Nougat Creme | &#128994; seit April 2019 | [Creme Nocciolata "Rigoni di Asiago"](https://www.amazon.de/Rigoni-Asiago-Milchfreie-Bio-Nuss-Nougat-Creme-Nocciolata-Rigoni-di-Asiago-Italienisches-Produkt/dp/B01HO4RF8E) | Ohne Plamöl (Leider noch mit Plastikdeckel) |
| Speise-Eis | &#128994; seit Juli 2019 | [Krups GVS241 Perfect Mix 9000 Eismaschine](https://www.krups.de/Speisen-zubereiten/Eismaschine/Perfect-Mix-9000/p/1500636693) | Selbst gemachtes Eis |
| Brot | &#128994; seit Juli 2019 | Bio Brot ohne chemische Fertigmischungen | Einkauf auf dem Böblinger Wochenmarkt bei Bäcker Wanner oder in der [Bäckerei Späth](http://www.biobaeckerei-spaeth.de/) |


## Getränke

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Stilles Wasser | &#128994; seit 2015 | [Stadtwerke Böblingen](https://www.stadtwerke-boeblingen.de/) | Leitungswasser aus dem Wasserhahn |
| Sprudel | &#128994; seit 2017 | [Sodastream](https://www.sodastream.de/produkte/wassersprudler/) | Vorher Sprudel nur in Glasflaschen von regionalem Anbieter. |
| Bier | &#128994; | [Schönbuch Braumanufaktur](https://www.braumanufaktur.com/) | Regionales Bier, daher geringe Transportwege. |
| Wein | &#128994; | [Diverse Sorten im BB Reformhaus Klett](http://www.reformhaus-klett.de/service-sortiment/) | Das Reformhaus in BB bietet eine größere Auswahl an Bio Weinen als z.B. der real Getränkemarkt |
| Strohhalme | &#128994; seit April 2019 | [halm.co](https://www.halm.co/de/) | Plastikstrohhalme durch Glasröhrchen ersetzt. |



## Hygieneartikel

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Duschen | &#128994; seit April 2019 | [Nablus Olivenölseife](http://www.nablussoaps.de/), [Aleppo Kernseife](https://www.kernseife.info/aleppo-seife/), [Share](https://www.share.eu/produkt/stueckseife-vanille-patchouli/) | Verwendung von ausschließlich [palmölfreien Seifen](https://utopia.de/bestenlisten/seife-ohne-palmoel/) |
| Rasieren | &#128994; seit August 2019 | [Savion Rasierseife](https://www.savion.de/Spezialseifen/Rasierseifen/Rasierseife.html) | Verwendung von ausschließlich [palmölfreien Seifen](https://utopia.de/bestenlisten/seife-ohne-palmoel/) |
| Shampoo | &#128994; seit Juni 2019| [Aleppo Kernseife](https://www.kernseife.info/aleppo-seife/) oder [Savion Haarwaschseife](https://www.savion.de/Haarwaschseifen/) | Verwendung von ausschließlich [palmölfreien Seifen](https://utopia.de/bestenlisten/seife-ohne-palmoel/) |
| Zahnbürsten | &#128994; seit April 2019 | [Hydrophil](https://hydrophil.com/) oder [Humble Brush](https://thehumble.co/humble-brush/) (bei [dm](https://www.dm.de/humble-brush-zahnbuerste-bambus-mittel-p7350075690402.html)) | Bambuszahnbürsten |
| Zahnpasta | &#128994; seit April 2019 | [Schüttgut Stuttgart](https://www.schuettgut-stuttgart.de/), [Ganz Ohne](http://www.ganz-ohne.de/) (Herrenberg) oder [Or-ganic](www.or-ganic.de) (Magstadt) | Zahnputztabletten aus dem Unverpacktladen oder [selbst gemacht](https://utopia.de/ratgeber/zahnpasta-selber-machen/) |
| Toilettenpapier | &#128994; seit Juni 2019 | [Satino Black](http://www.cradle-to-cradle.eu/satino-black-toilettenpapier-weiss-2-lagig-4-rollen-4-x-400-blatt/a-731/) oder [Goldeimer](https://www.goldeimer.de/) | Umstieg auf Recyclingpapier. Bei Goldeimer ist die Umverpackung leider noch in Plastik. Satino Black hat eine Cradle-2-Cradle Zertifizierung und scheint damit besonders nachhaltig zu sein, ist aber leider extrem teuer. |
| Deo | &#128994; | [Selbstgemacht](https://www.smarticular.net/8-rezepte-fuer-guenstige-und-gesunde-deodorants-zum-selbermachen/) | Deo aus eigener Herstellung |
| Sonnencreme | &#128308; |  | im Test [Suntribe](https://suntribesunscreen.com/) -> sehr teuer |
| Menstruationstasse | &#128994; seit April 2019 | [Menstruationstasse](https://de.wikipedia.org/wiki/Menstruationstasse) | Kein Abfall mehr! |
| Haar Spülung | &#128994; seit April 2019 | [Saure Rinse](https://utopia.de/ratgeber/saure-rinse-fuer-die-haare-so-funktionierts/) |  |
| Haar Packung | &#128994; seit April 2019 |  | Kur mit Kokosöl |
| Bodylotion & Gesichtscreme & Haaröl | &#128994; seit April 2019 | [Vanille Body-Butter](https://www.pinterest.de/pin/740208888732581497/?nic=1) | Selbst gemacht |
| Handcreme | &#128308; |  | Selbst gemachten Alternativen zu fettig. Öl zieht nicht in die Haut ein. |
| Lippenbalsam | &#128994; | ? | Selbst hergestellt. |


## Reinigung

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Waschmittel | &#128308; |  | Pulver in Pappkarton-Verpackung -> Ziel: Selbst herstellen! |
| Spülmittel | &#128994; seit Mai 2019 |  | Selbst hergestellt. Reinigungswirkung nicht zufriedenstellend. |
| Badreiniger | &#128308; |  | auf der Suche. Selbst gemachter Essigreiniger fehlte die Fettlösekraft, insb. um die Seifenrückstände zu entfernen |
| Küchenreiniger | &#128308; |  | auf der Suche |
| Klarspüler (Spülmaschinen) | &#128994; |  | Haushaltsessig in der Glasflasche |
| Spülmaschinentabs | &#128992; | [denkmit Geschirr Reiniger](https://utopia.de/produkt/denkmit-geschirr-reiniger-tabs-nature/) | Alle Tabs sind in eine Folie eingewickelt, die sich aber angeblich vollständig beim Spülvorgang zersetzt. |


## Papier

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Briefkastenwerbung | &#128994; seit Juni 2019 | [Robinsonliste](https://www.robinsonliste.de/) [Plastikpost](https://www.plastikpost.de/) | Aufkleber auf Briefkasten, Eintrag in Robinsonliste, Eintrag bei [Plastikpost](https://www.plastikpost.de/) und Widerspruch bei allen Werbetreibenden, die dialogPost verwenden (Adresse für Widerspruch findet man auf den Prospekten im Kleingedruckten). |
| Druckerpapier | &#128994; seit 2019 |  | Verwendung von FSC zertifiziertem Recyclingpapier, Vermeidung von Ausdrucken. |
| Amtsblatt Böblingen | &#128992; |  | Noch in Papierform, digitale Version verfügbar, aber keine Benachrichtigungsfunktion über neue Ausgaben. Benachrichtigungsfunktion beim Verlag angefragt. Auf Recyclingpapier gedruckt. |
| Bücher | &#128992; |  | Echte Papierbücher nur noch sehr selten ([Buch7.de](https://buch7.de/)) |
| Versicherungen | &#128994; seit Juni 2019 |  | Wo möglich, Papierversand durch Onlinebenachrichtigung ersetzt. |
| Banken | &#128994; seit Juni 2019 |  | Wo möglich, Papierversand durch Onlinebenachrichtigung ersetzt. Postbank gibt an, dass sie rechtlich verpflichtet sei, einmal im Jahr Papierausdrucke für Sparkonten zu versenden. |
| Stadtwerke BB, Landratsamt BB | &#128308; |  | Versand der Rechnungen weiterhin in Papierform |


## Kinder

Grundsätzlich kaufen wir fast Alles für die Kinder gebraucht auf Flohmärkten ein. Ein Neukauf ist selten, weil die Kinder viel zu schnell rauswachsen. Die Flohmärkte in der Umgebung von Stuttgart bieten dabei eine große Auswahl.


## Kleidung

| Kategorie | Status | Links | Kommentar |
|---|---|---|---|
| Kleidung | &#128308; |  | Bisher keine nachhaltige Kleidung. Mit diesem Thema wollen wir uns als nächstes beschäftigen. |
| Sportbekleidung | &#128308; |  | Bisher keine nachhaltige Kleidung gekauft. Gibt es überhaupt nachhaltige Sportbekleidung? |

