---
layout: single
title: EthikBank - Faires Geld
date: '2019-05-10T00:23:00.000+02:00'
author: Michael Rumpf
tags:
- faires geld
- ökologische konten
- klimaschutz
categories:
---

Seit Anfang Mai 2019 haben wir ein Konto bei der  [Ethikbank](https://www.ethikbank.de/) und stellen gerade alle Abbuchungsgenehmigungen auf das neue Konto um.

Unter den nachahltigen Banken schien uns das Engagement der deutschen Ethikbank am unterstützungswertesten:

"Die strengen Anlagekriterien der EthikBank umfassen umfangreiche Negativkriterien, so sind etwa Unternehmen oder Staaten tabu, die mit der Herstellung von Waffen und Atomkraftwerken, mit Kinderarbeit oder genveränderten Pflanzen Gewinne machen. Darüber hinaus müssen eine Reihe von Positivkriterien erfüllt sein – etwa eine umfassende Umweltpolitik, die Einhaltung der Menschenrechte oder die Förderung von Vielfalt und Gleichberechtigung der Mitarbeiter." (Quelle: [Utopia.de](https://utopia.de), siehe auch [Anlagekriterien der Ethikbank](https://www.ethikbank.de/die-ethikbank/anlagekriterien.html)

Die Transparenz mit der die Ethikbank Investitionen tätigt ist vorbildlich, siehe [Gläserne Bank](https://www.ethikbank.de/die-ethikbank/glaeserne-bank.html#c13154)

Das Girokonto bei der Postbank wird gekündigt, sobald die Umstellung der Abbucher beendet ist!

Am meisten Sorgen bereitete die Umstellung aller Abbucher auf das neue Konto. Diese Sorge war,
im nachinein betrachtet, völlig unbegründet, da man die Änderung der Kontodaten bei allen
Abbuchern sehr schnell und unproblematisch per Email beauftragen konnte.