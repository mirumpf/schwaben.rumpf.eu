---
layout: single
title: Nachhaltiges Toilettenpapier
date: '2019-03-12T00:23:00.000+02:00'
author: Michael Rumpf
tags:
- toilettenpapier
- recyclingpapier
- kein plastik
categories:
---

Verwendet man Toilettenpapier aus Frischfasern, stellt sich die Frage nach
der Erzeugung dieser Frischfasern. Viele der Papiere belegen mit
einem FSC Zertifikat, dass es sich dabei um Papier aus einer nachhaltigen Forstwirtschaft handelt. Trotzdem werden bei Frischfasern Bäume gefällt und die Abholzung der Urwälder nimmt trotz FSC Zertifikaten immer weiter an Geschwindigkeit zu.

Bei Recyclingpapier gibt es das Problem der "Bisphenol A" (BPA) Belastung.
Diese tritt auf, weil z.B. Kassenzettel aus Thermopapier, welches mit BPA
beschichtet ist, in die Recyclingkreisläufe gelangen (Kassenbons gehören
in den Restmüll!).
Laut Bundesumweltamt sind die Konzentrationen zwar angeblich unbedenklich
und trotzdem hat die EU das Beschichten von Thermopapier mit BPA ab 2020 
verboten (siehe [EU erkennt Bisphenol A als besonders besorgniserregend an](https://www.umweltbundesamt.de/presse/pressemitteilungen/eu-erkennt-bisphenol-a-als-besonders)).
Man geht davon aus, dass mit diesem Verbot die Belastung im Papierkreislauf 
weiter verringert wird.

Neben der Entscheidung zum Papier, bleibt auch noch die Frage nach der
korrekten Umverpackung. Diese besteht aus Transportgründen meistens 
aus Plastikfolie. Von [Danke](http://danke.de/toilettenpapier) gab es vor Jahren
Recycling Toilettenpapier, welches in [Recyclingpapier](https://browse.startpage.com/do/show_picture.pl?l=english&rais=1&oiu=https%3A%2F%2Fwww.picclickimg.com%2Fd%2Fl400%2Fpict%2F302667081291_%2FNeu-mint-O-153795-Danke-Toilettenpapier-Aufl-1000.jpg&sp=5612631a82934c88264643e3da0ad1e2&rl=NONE&t=default) verpackt war.
Auf Nachfrage beim Hersteller, warum es das heute nicht mehr gibt, wurde
wie folgt argumentiert: "Wir haben uns intensiv mit dem Thema alternativer Verpackungen befasst. Leider ist es uns bis jetzt nicht gelungen, eine Lösung zu finden, die großtechnisch umsetzbar und ökonomisch vertretbar ist und darüber hinaus noch den notwendigen Gebrauchsnutzen (Schutz der Ware beim Transport und Verkauf) gewährleistet."

Unser Ranking ist nach diesen Erkenntnissen wie folgt:
1. [Smooth Panda](https://smoothpanda.de/) (Bambuspapier, im Karton)
2. [Satino Black](https://www.wepa.nl/en/brands/5195/satino-black.html) (Recyclingpapier, im Karton)
3. [Goldeimer](https://www.goldeimer.de/) (Recyclingpapier, Granic-Folie mit 30% Kreide)
4. [Danke](http://danke.de/toilettenpapier) (Recyclingpapier, Polyethylenverpackungen)

Vollständig papierfrei würde es mit der Podusche von [Happy Po](https://happypo.de/), ist aber
gewöhnungsbedürftig, insbesondere bei kaltem Wasser ;)
