---
layout: single
title: EWS Schönau - Die Stromrebellen
date: '2019-02-22T00:23:00.000+02:00'
author: Michael Rumpf
tags:
- grüner strom
- ews schönau
- ökostrom
categories:
---

Bei unserem Einzug ins Haus 2009 erfolgte die Stromlieferung über die EnBW
und wir haben den Vertrag ohne viel nachzudenken übernommen.

Die Besonderheit in unserem Haus sind die beiden Stromzähler, einen für den 
Hausstrom (EnBW Komfort) und einen weiteren für unsere Erdwärmepumpe (EnBW 
WärmePro).  Der Hausstromtarif war ein normaler Tarif, also ohne besondere 
Vergünstigungen. Der Wärmepumpentarif war ein 2 Tarifzähler (HT/NT) mit 
höherem Preis für die HT Phase und einem günstigeren Betrag für die NT Phase. 
Insgesamt war der Preis für die kWh im Wärmepumpentarif deutlich günstiger 
als für die kWh Hausstrom.

Die erste grundlegende Anpassung kam vor ca. 2 Jahren, als das 
Dualpreis-System im Wärmepumpentarif abgeschafft wurde. Der NT Preis wurde 
gestrichen und der HT Preis für alle verbrauchten kWh zugrunde gelegt.

Nach den üblichen Preisanpassungen der letzten Jahre liegt der Preis seit
dem 01.01.2019 für die kWh Hausstrom bei 29,99 Cent und bei
21,94 Cent pro kWh für die kWh Wärmepumpenstrom.

Ein erster Vergleich von sog. "Ökostromtarifen" hat gezeigt, dass wir 
mindestens  kostenneutral wechseln könnten. Aber welcher der vielen tausend 
Tarife ist  wirklich nachhaltig?
Der letzte Test der Zeitschrift Ökotest gibt dazu Auskunft:
[Öko-Strom im Test](https://www.oekotest.de/bauen-wohnen/Oeko-Strom-im-Test-Das-sind-die-besten-Anbieter_111510_1.html)
Dort hat man die [EcoTopTen Liste für Stromanbieter](https://www.ecotopten.de/strom/oekostrom-tarife)
des Freiburger Öko-Instituts als Basis genommen. Auf diese Liste kommen
nur Unternehmen, die einen ökologischen Zusatznutzen aufweisen, z.B.
der Bau ökologischer Kraftwerke. Von diesen 34
Anbietern wurden die abgewertet, die in ihrem Firmenverbund auch 
Stromprodukte auf Basis von Atom- oder Kohlestrom anbieten. Übrig geblieben
sind 19 Anbieter, die als "sehr gut" eingestuft wurden und die
keine Verbindungen zu den traditionellen Stromerzeugern haben.

Unter den 19 Testsiegern hat uns die 
[EWS Schönau](https://www.ews-schoenau.de/) 
am besten gefallen. Die konsequente Ausrichtung auf erneuerbare Energien,
das Genossenschaftsmodell, die Förderung von erneuerbaren Energieanalagen
und die geographische Nähe gaben den Ausschlag von der EnBW zur EWS Schönau
zu wechseln.

Ende März 2019 wird also der Hausstrom umgestellt (29,45 Cent/kWh, inkl. 2 
Sonnencent) und Ende Oktober 2019 folgt dann der Wärmepumpenstrom (23,09 
Cent/kWh). Wir haben uns außerdem entschieden 2 "Sonnencent" zu zahlen, 
um damit weitere Investitionen in erneuerbare Energien zu fördern.

Der Wechsel gestaltet sich bisher als problemlos. Die EWS Schönau hat die
Kommunkation mit der EnBW übernommen und die Kündigungen termingerecht
in Auftrag gegeben. Wir werden also von der Umstellung so gut wie
nichts mitbekommen.

Ab März/November 2019 wird dann endlich grüner Strom aus der Steckdose
kommen und mit den 2 Sonnencent unterstützen wir die EWS Schönau, die
Energiewende in Deutschland weiter voranzutreiben.